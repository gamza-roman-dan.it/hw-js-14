"use strict"

/*

1-
вшдмінність полягає у тому, що localStorage продовжує своє існування
навіть після того як користувач залишить сайт. на відмінну від sessionStorage, він зникає піісля закриття сайту.

2-
певно взагалі краще не зберігати дуже чутливу інформацію у localStorage, особли якщо підключені скрипти зі сторонніх джерел.

3-
ці данні просто очищуються


---------------------------------------------------------------------------------------------------------------------
*/

const btn = document.querySelector(".change-theme");
const bodyTeg = document.body.classList;

if (localStorage.getItem('value')) {
    bodyTeg.value = localStorage.getItem('value');
}

btn.addEventListener("click",() => {
    document.body.classList.toggle("black-theme");
    !! bodyTeg.value ? localStorage.setItem('value' , bodyTeg.value) : localStorage.removeItem('value');
});